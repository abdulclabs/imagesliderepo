//
//  ViewController.swift
//  ImageSlide
//
//  Created by Click Labs on 1/13/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var noticeLabel: UILabel!
    
    @IBOutlet weak var myImageView: UIImageView!
    
    @IBOutlet weak var inputField: UITextField!
    
    //when button is clicked...
    @IBAction func getImage(sender: AnyObject) {
        
         //unhide image View..
         myImageView.hidden = false
        
         var guess = inputField.text.toInt();
        
         //set image...
         let image1 = UIImage(named: "images/img1.jpeg")
         let image2 = UIImage(named: "images/img2.jpeg")
         let image3 = UIImage(named: "images/img3.jpeg")
         let image4 = UIImage(named: "images/img4.jpeg")
         let image5 = UIImage(named: "images/img5.jpeg")
         let image6 = UIImage(named: "images/img6.jpeg")
         let image7 = UIImage(named: "images/img7.jpeg")
         let image8 = UIImage(named: "images/img8.jpeg")
         let image9 = UIImage(named: "images/img9.jpeg")
        let image10 = UIImage(named: "images/img10.jpeg")
        
        //check whether input value is nil or not, if not nil then...
        if((inputField.text) != nil){
            if(guess == 1){
                
                myImageView.image = UIImage(named: "img1.jpeg")//THIS IS NOT WORKING
                inputField.resignFirstResponder();// hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            }
            else if(guess == 2)
            {
                myImageView.image=UIImage(named: "img2.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else if(guess == 3)
            {
                myImageView.image=UIImage(named: "img3.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else if(guess == 4)
            {
                myImageView.image=UIImage(named: "img4.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else if(guess == 5)
            {
                myImageView.image=UIImage(named: "img5.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else if(guess == 6)
            {
                myImageView.image=UIImage(named: "img6.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
           
            }
            else if(guess == 7)
            {
                myImageView.image=UIImage(named: "img7.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else if(guess == 8)
            {
                myImageView.image=UIImage(named: "img8.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else if(guess == 9)
            {
                myImageView.image=UIImage(named: "img9.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else if(guess == 10)
            {
                myImageView.image=UIImage(named: "img10.jpeg")
                inputField.resignFirstResponder();//hides keyboard
                
                noticeLabel.text = ""//set label to nil...
            
            }
            else
            {
                //unhide image View..
                myImageView.hidden = true
                
                noticeLabel.text = "Enter the number between 1 to 10..."
                inputField.resignFirstResponder();
            
            }
            
            
        }
        else
        {
            //unhide image View..
            myImageView.hidden = true
            
            // hides keyboard
            inputField.resignFirstResponder();
        
        }
        
    }
    //hide the keyboard when touches outside of the keyboard...
    //hide the keyboard when touches outside of the keyboard...
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        }


}

